
//Abbigliamento
function Abbigliamento(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",
        success: function(response) {
            let content="";

            for(let item of response)
            {
                if(item.categoria == "abbigliamento")
                {
                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                }
            }
            $("#t_Abbigliamento").html(content);

        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}






// Calzature
function Calzature(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",

        success: function (response) {
            let content="";

            for(let item of response){
                if(item.categoria == "calzature")
                {
                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                }
            }
            $("#t_Calzature").html(content);

        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}






// Borse
function Borse(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",

        success: function (response) {
            let content="";

            for(let item of response)
            {
                if(item.categoria == "borse")
                {
                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                }
            }
            $("#t_Borse").html(content);

        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}






// Uomo
function Uomo(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",

        success: function (response) {
            let content="";

            for(let item of response)
            {
                if(item.genere == "uomo")
                {
                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                }
            }
            $("#t_Uomo").html(content);

        },error: function(errore){
            console.log(errore)

        }
    });
}






// Donna
function Donna(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",

        success: function (response) {
            let content="";

            for(let item of response)
            {
                if(item.genere == "donna")
                {
                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                }
            }
            $("#t_Donna").html(content);

        },error: function(errore){
            console.log(errore)

        }
    });
}






// Minori di 30€
function m_Trenta(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",

        success: function (response) 
        {
            let content="";

            for(let item of response)
            {
                if(item.prezzo <= 30)
                {
                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                }
            }
            $("#t_Offerta").html(content);

        },error: function(errore)
        {
            console.log(errore)

        }
    });
}






//Maggiori di 30€
function M_Trenta(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",

        success: function(response) {
            let content="";

            for(let item of response)
            {
                if(item.prezzo >= 30)
                {
                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                }
            }
            $("#t_Max").html(content);

        },error: function(errore){
            console.log(errore)

        }
    });
}






// Top Articoli
function Top(){
    $.ajax({

        url: "prodotti.json",
        method: "GET",

        success: function (response) {
            let Top=Array.from(response).sort((a, b) => parseFloat(b.venduti) - parseFloat(a.venduti)).slice(0,3);

            let content="";

            for(let item of Top)
            {

                    content+=
                    `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;

            }
            $("#t_Top").html(content);

        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}





//STAMPA TUTTO
$(document).ready(function() {

    Abbigliamento();
    Calzature();
    Borse();
    Uomo();
    Donna();
    m_Trenta();
    M_Trenta();
    Top();
});
