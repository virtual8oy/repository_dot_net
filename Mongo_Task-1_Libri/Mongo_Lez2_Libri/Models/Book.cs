﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Lez2_Libri.Models
{
    //TODO MAUIUSCOLE SU DB
    public class Book
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }



        
        [Required]
        [MaxLength(255)]
        public string Nome { get; set; }


        
        [Required]
        [MaxLength(255)]

        public string Descrizione { get; set; }



        [MaxLength(255)]
        [Required]
        public string Codice { get; set; }



        [Required]
        public int Quantita { get; set; }


        
        
        public ObjectId Category { get; set; }

        [Required]
        [BsonIgnore]
        public string Categoria { get; set; }  //Titolo della categoria da cui si estrapolera l'object id



        [BsonIgnore]
        public Category InfoCategoria { get; set; }

    }
}
