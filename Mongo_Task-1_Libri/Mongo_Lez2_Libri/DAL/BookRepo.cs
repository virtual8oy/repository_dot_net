﻿using Mongo_Lez2_Libri.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Lez2_Libri.DAL
{
    public class BookRepo : InterfaceRepo<Book>
    {

        private IMongoCollection<Book> libri;

        private string strConne;
        private string strDatab;

        #region BookRepo
        public BookRepo(string strConnessione, string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            if (libri == null)

                libri = db.GetCollection<Book>("Books");

            strConne = strConnessione; 
            strDatab = strDatabase;
        }
        #endregion



        #region Delete
        public bool Delete(ObjectId varId)
        {
            throw new NotImplementedException();
        }
        #endregion



        #region GetAll
        public IEnumerable<Book> GetAll()
        {
            List<Book> elenco = libri.Find(FilterDefinition<Book>.Empty).ToList();


            CategoryRepo tempRepo = new CategoryRepo(strConne, strDatab);
            foreach(var item in elenco)
            {
                item.InfoCategoria = tempRepo.GetById(item.Category);
            }
            return elenco;
        }
        #endregion



        #region GetById
        public Book GetById(ObjectId varId)
        {

            throw new NotImplementedException();
        }
        #endregion



        #region Insert
        public bool Insert(Book t)
        {
            Book temp = libri.Find(d => d.Codice == t.Codice).FirstOrDefault();
            if (temp == null)
            {
                //Caso in cui la rest mi invia il titolo categoria
                //prima cosa controllo se esiste
                CategoryRepo tempRepo = new CategoryRepo(strConne, strDatab);
                Category tempCate = tempRepo.FindByTitle(t.Categoria);

                if (tempCate != null)
                {
                    t.Category = tempCate.DocumentID;
                    libri.InsertOne(t);
                    if (t.DocumentID != null)
                        return true;
                }
            }
            return false;
        }
        #endregion



        #region
        public bool Update(Book t)
        {
            throw new NotImplementedException();
        }
        #endregion

    }

}
