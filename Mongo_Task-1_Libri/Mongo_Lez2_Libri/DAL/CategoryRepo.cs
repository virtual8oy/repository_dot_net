﻿using Mongo_Lez2_Libri.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Lez2_Libri.DAL
{
    public class CategoryRepo : InterfaceRepo<Category>
    {

        private IMongoCollection<Category> categorie;


        #region Interfaccia connessione CategoryRepo
        //Implemento l'interfaccia automaticamente
        public CategoryRepo(string strConnessione, string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            if(categorie == null)

                categorie = db.GetCollection<Category>("Categories");
        }
        #endregion



        #region Delete
        public bool Delete(ObjectId varId)
        {
            throw new NotImplementedException();
        }
        #endregion


        #region GetAll
        public IEnumerable<Category> GetAll()
        {
            return categorie.Find(FilterDefinition<Category>.Empty).ToList();
        }
        #endregion


        #region GetById
        public Category GetById(ObjectId varId)
        {
            return categorie.Find(d => d.DocumentID == varId).FirstOrDefault();
        }
        #endregion


        #region Insert
        public bool Insert(Category t)
        {
            //Faccio subito la insert
            Category temp = categorie.Find(d => d.Codice == t.Codice).FirstOrDefault();
            if(temp == null)
            {
                //todo riversa il dato sul db
                categorie.InsertOne(t);
                if(t.DocumentID != null) //id lo assegna il db

                    return true;
            }
            return false;
        }
        #endregion


        #region Update
        public bool Update(Category t)
        {
            throw new NotImplementedException();
        }
        #endregion


        #region FindByTitle
        public Category FindByTitle(string varTitolo)
        {
           return categorie.Find(d => d.Titolo == varTitolo).FirstOrDefault();
        }
        #endregion

    }
}
