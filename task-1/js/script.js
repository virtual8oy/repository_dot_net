//Inserimento Dati Pagina Principale
function inserisci(){
    
    let varTitolo = document.getElementById("inTitolo").value;
    let varAutore = document.getElementById("inAutore").value;
    let varCodice = document.getElementById("inCodice").value;
    let varPrezzo = document.getElementById("inPrezzo").value;
    let varQuantita = document.getElementById("inQuantita").value;

    let prod = {
        id: calcolaId(),
        titolo: varTitolo,
        autore: varTitolo,
        codice: varCodice,
        prezzo: parseFloat(varPrezzo),
        quantita: parseInt(varQuantita),
    }

    if( varTitolo.length == 0 ||
        varAutore.length == 0 ||
        varCodice.length == 0 ||
        varPrezzo.length == 0 ||
        varQuantita.length == 0 ){
        alert("Non puoi inserire un prodotto inesistente!");
        return;
    }

    if(isInElenco(varCodice)){
        alert("Non puoi inserire un prodotto già esistente!");
        return;
    }

    elenco.push(prod);
    localStorage.setItem("Biblioteca", JSON.stringify(elenco));
    alert("Libro registrato")

    document.getElementById("inCodice").focus()
}




function search(){
    let input = $('#searchbox').val(); //Jquery
    let contenuto = "";
    $("#contenuto-tabella").html("");  //Jquery

    for(let [index, item] of elenco.entries()){

        if(item.titolo.toLowerCase().includes(input.toLowerCase())){
            contenuto += `
            <tr>
                <td>${item.id}</td>
                <td>${item.titolo}</td>
                <td>${item.autore}</td>
                <td>${item.codice}</td>
                <td>${item.prezzo}</td>
                <td>${item.quantita}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="elimina(${item.Id})">
                        <i class="fa-solid fa-trash text-danger"></i>
                    </button>
                    <button type="button" class="btn btn-outline-warning" onclick="apriModale(${item.Id})">
                        <i class="fa-solid fa-pencil text-warning"></i>
                    </button>
                </td>
            </tr>
        `;
        }        
        
    }
    
    contenuto += calcolo();
    $("#contenuto-tabella").html(contenuto); //Jquery
    
    
}




//Cancella ricerca
function calcellaRicerca(){
   $('#searchbox').val(""); //Jquery
   search();

}




//Calcolo Prezzo
function calcolo(){
    let htmlCalcolo = "";
    let calcolo = 0;
    for(let item of elenco){
        calcolo += item.quantita * item.prezzo;
    }
    if (calcolo > 0) {
        htmlCalcolo = `
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>${calcolo}&euro;</td>
            </tr>
            `;
             
    }
    return htmlCalcolo;
}





//Verifica Codice doppione
function isInElenco(varCodice){
    for(let [index, item] of elenco.entries()){
        if(item.codice == varCodice)
            return true;
    }

    return false;
}




//Generatore di ID
function calcolaId(){
    if(elenco.length != 0)
        return elenco[elenco.length - 1].id + 1;

    return 1;
}




//Bottone Elimina Articoli
function elimina(varId){
    for(let [index, item] of elenco.entries()){
        if(item.id == varId)
            elenco.splice(index, 1)
    }

    localStorage.setItem("Biblioteca", JSON.stringify(elenco));
    stampaProdotti();
}




//Bottone Apertura Modale
function apriModale(varId){
    for(let [index, item] of elenco.entries()){
        if(item.id == varId){
            document.getElementById("update_id").value = item.id;
            document.getElementById("update_titolo").value = item.titolo;
            document.getElementById("update_autore").value = item.autore;
            document.getElementById("update_codice").value = item.codice;
            document.getElementById("update_prezzo").value = item.prezzo;
            document.getElementById("update_quan").value = item.quantita;

        }
    }

    $("#modaleModifica").modal('show');
}




//Bottone Chiusura Modale
function chiudi(){
    $("#modaleModifica").modal('hide');

    stampaProdotti();
}



//Bottone Modifica Dati
function aggiorna(){
    let var_Id = document.getElementById("update_id").value;
    let var_Titolo = document.getElementById("update_titolo").value;
    let var_Autore = document.getElementById("update_autore").value;
    let var_Codice = document.getElementById("update_codice").value;
    let var_Prezzo = document.getElementById("update_prezzo").value;
    let var_Quantita = document.getElementById("update_quan").value;

    let oggetto = {
        id: var_Id,
        titolo: var_Titolo,
        autore: var_Autore,
        codice: var_Codice,
        prezzo: var_Prezzo,
        quantita: var_Quantita, //parseint(var_Quantita)
    }

    for(let [index, item] of elenco.entries()){
        if(item.id == oggetto.id){
            item.titolo = oggetto.titolo;
            item.autore = oggetto.autore;
            item.codice = oggetto.codice;
            item.prezzo = oggetto.prezzo;
            item.quantita = oggetto.quantita;

            alert("Modifica Eseguita");
        }
    }
    
    $("#modaleModifica").modal('hide');

    stampaProdotti();
}




//Lista Articoli
function stampaProdotti(){
    let contenuto = "";

    for(let [index, item] of elenco.entries()){
        contenuto += `
            <tr>
                <td>${item.id}</td>
                <td>${item.titolo}</td>
                <td>${item.autore}</td>
                <td>${item.codice}</td>
                <td>${item.prezzo}</td>
                <td>${item.quantita}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="elimina(${item.id})">
                        <i class="fa-solid fa-trash text-danger"></i>
                    </button>
                    <button type="button" class="btn btn-outline-warning" onclick="apriModale(${item.id})">
                        <i class="fa-solid fa-pencil text-warning"></i>
                    </button>
                </td>
            </tr>
        `;
    }

    contenuto += calcolo();
    document.getElementById("contenuto-tabella").innerHTML = contenuto;

}




if(localStorage.getItem("Biblioteca") == null)
    localStorage.setItem("Biblioteca", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("Biblioteca"));




if(document.getElementById("contenuto-tabella") != null)
    stampaProdotti();



