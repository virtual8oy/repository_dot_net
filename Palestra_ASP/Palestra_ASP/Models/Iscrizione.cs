﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Models
{
    [Table("Iscrizioni")]
    public class Iscrizione
    {
        
        //[Column("IdCorso")]
        
        [ForeignKey("IdCorso")]
        public Corso Corso { get; set; }




        [ForeignKey("IdUtente")]
        public Utente Utente { get; set; }

    }
}
