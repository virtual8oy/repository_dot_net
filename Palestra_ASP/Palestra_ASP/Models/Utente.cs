﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Models
{
    [Table("Utenti")]
    public class Utente
    {
        [Column("Id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }




        [NotMapped]
        public string Cod_U { get; set; }
        public Utente()
        {
            Cod_U = Guid.NewGuid().ToString();
        }




        [Column("Nome")]
        [Required(ErrorMessage = "Il campo Nome è obbligatorio")]
        [StringLength(50, ErrorMessage = "Il campo Nome è troppo lungo")]
        public string Nome { get; set; }


        [Column("Cognome")]
        [Required(ErrorMessage = "Il campo Nome è obbligatorio")]
        [StringLength(50, ErrorMessage = "Il campo Cognome è troppo lungo")]
        public string Cognome { get; set; }


        [Column("Indirizzo")]
        [Required(ErrorMessage = "Il campo Indirizzo è obbligatorio")]
        public string Indirizzo { get; set; }


        [Column("D_Nascita")]
        [Required(ErrorMessage = "Il campo 'Data di Nascita' è obbligatorio")]
        public string D_Nascita { get; set; }


        [Column("Username")]
        [Required(ErrorMessage = "Il campo Username è obbligatorio")]
        [MinLength(3, ErrorMessage = "Il campo Username è troppo corto")]
        [StringLength(50, ErrorMessage = "Il campo Username è troppo lungo")]
        public string Username { get; set; }


        [Column("Password")]
        [Required(ErrorMessage = "Il campo Password è obbligatorio")]
        [StringLength(50, ErrorMessage = "Il campo Password è troppo lungo")]
        public string Password { get; set; }


        [NotMapped]
        [Column("VerificaPwd")]
        [Required(ErrorMessage = "Il campo Verifica password è obbligatorio")]
        [StringLength(50, ErrorMessage = "Il campo Verifica password è troppo lungo")]
        public string VerificaPwd { get; set; }




        public ICollection<Corso> Corsi { get; set; }








    }
}
