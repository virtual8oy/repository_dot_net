﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Models
{
    [Table("Corsi")]
    public class Corso
    {
        [Column("Id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int Id { get; set; }



        [NotMapped]
        public string Cod_C { get; set; }
        public Corso()
        {
            Cod_C = Guid.NewGuid().ToString();
        }




        [Column("Titolo")]
        [Required]
        [StringLength(50)]
        public string Titolo { get; set; }



        [Column("Descrizione")]
        [Required]
        [StringLength(255)]
        public string Descrizione { get; set; }



        [Column("Codice")]
        [Required]
        public string Codice { get; set; }


        [Column("Data")]
        [Required]
        public DateTime Data { get; set; }




        
        public ICollection<Utente> Utenti { get; set; }
    }
}
