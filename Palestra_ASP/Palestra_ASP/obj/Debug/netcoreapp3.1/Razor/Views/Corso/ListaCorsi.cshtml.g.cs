#pragma checksum "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "128e7f9ed005440d4e3bb8b4167a7336ed66a4f6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Corso_ListaCorsi), @"mvc.1.0.view", @"/Views/Corso/ListaCorsi.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"128e7f9ed005440d4e3bb8b4167a7336ed66a4f6", @"/Views/Corso/ListaCorsi.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b853c2693020103458b52d8246f923ca3fd89014", @"/Views/_ViewImports.cshtml")]
    public class Views_Corso_ListaCorsi : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Palestra_ASP.Models.Corso>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 3 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
   Layout = "_Layout"; 

#line default
#line hidden
#nullable disable
            WriteLiteral("\n<h1>Corsi Disponibili</h1>\n\n<table class=\"table table-striped\">\n    <thead>\n        <tr>\n            <th>Titolo</th>\n            <th>Descrizione</th>\n            \n        </tr>\n    </thead>\n    <tbody>\n");
#nullable restore
#line 16 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
         foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\n                    <td>");
#nullable restore
#line 19 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
                   Write(item.Titolo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                    <td>");
#nullable restore
#line 20 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
                   Write(item.Descrizione);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n\n                    <td>\n                        <a");
            BeginWriteAttribute("href", " href=\"", 481, "\"", 517, 3);
            WriteAttributeValue("", 488, "/Corso/Dettaglio/", 488, 17, true);
#nullable restore
#line 23 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
WriteAttributeValue("", 505, item.Cod_C, 505, 11, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 516, "", 517, 1, true);
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 23 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
                                                           Write(item.Cod_C);

#line default
#line hidden
#nullable disable
            WriteLiteral(" e</a>\n                    </td>\n                    <td>");
#nullable restore
#line 25 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
                   Write(item.Titolo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                    <td>");
#nullable restore
#line 26 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
                   Write(item.Descrizione);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                    <td>");
#nullable restore
#line 27 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
                   Write(item.Codice);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                    <td>");
#nullable restore
#line 28 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
                   Write(item.Data);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                    <td>\n                        <button type=\"button\" class=\"btn btn-danger\"");
            BeginWriteAttribute("onclick", " onclick=\"", 827, "\"", 863, 3);
            WriteAttributeValue("", 837, "eliminaCor(\'", 837, 12, true);
#nullable restore
#line 30 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
WriteAttributeValue("", 849, item.Cod_C, 849, 11, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 860, "\')", 861, 3, true);
            EndWriteAttribute();
            WriteLiteral(">Elim.</button>\n                    </td>\n                </tr>\n");
#nullable restore
#line 33 "C:\Users\Valerio\source\repos\Palestra_ASP\Palestra_ASP\Views\Corso\ListaCorsi.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("\n    </tbody>\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Palestra_ASP.Models.Corso>> Html { get; private set; }
    }
}
#pragma warning restore 1591
