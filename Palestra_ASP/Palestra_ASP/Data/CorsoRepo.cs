﻿using Palestra_ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Data
{
    public class CorsoRepo : InterfaceRepo<Corso>
    {
        private readonly AppDbContext _context;

        public CorsoRepo(AppDbContext con)
        {
            _context = con;
        }





        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }





        public IEnumerable<Corso> GetAll()
        {
            return _context.ElencoCorsi.ToList();
        }





        public Corso GetById(int varId)
        {
            throw new NotImplementedException();
        }

        public Corso GetByUsernameAndPassword(string username, string password)
        {
            throw new NotImplementedException();
        }




        public Corso GetFromCodice(string cod_C)
        {
            return _context.ElencoCorsi.Where(o => o.Cod_C == cod_C).FirstOrDefault();
        }




        public Corso Insert(Corso t)
        {
            _context.ElencoCorsi.Add(t);
            _context.SaveChanges();

            return t;
        }




        public bool Update(Corso t)
        {
            throw new NotImplementedException();
        }
    }
}
