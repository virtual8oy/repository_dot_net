﻿using Microsoft.EntityFrameworkCore;
using Palestra_ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }


        public DbSet<Corso> ElencoCorsi { get; set; }
        public DbSet<Utente> ElencoUtenti { get; set; }
    }
}
