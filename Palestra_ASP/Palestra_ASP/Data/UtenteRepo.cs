﻿using Palestra_ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Data
{
    public class UtenteRepo : InterfaceRepo<Utente>
    {

        private readonly AppDbContext _context;




        public UtenteRepo(AppDbContext con)
        {
            _context = con;
        }






        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }





        public IEnumerable<Utente> GetAll()
        {
            return _context.ElencoUtenti.ToList();
        }





        public Utente GetById(int varId)
        {
            throw new NotImplementedException();
        }





        public Utente Insert(Utente t)
        {
            _context.ElencoUtenti.Add(t);
            _context.SaveChanges();

            return t;
        }



        public Utente GetByUsernameAndPassword(string username, string password)
        {
            var all = _context.ElencoUtenti;

            all.Where(o => o.Username == username);
            all.Where(o => o.Password == password);

            return all.FirstOrDefault();

        }



        


        public Utente GetFromCodice(string cod_U)
        {
            return _context.ElencoUtenti.Where(o => o.Cod_U == cod_U).FirstOrDefault();
        }

        


        public bool Update(Utente t)
        {
            throw new NotImplementedException();
        }

        
    }
}
