﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Palestra_ASP.Data;
using Palestra_ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Controllers
{
    public class UtenteController : Controller
    {

        private readonly InterfaceRepo<Utente> _repositoryUte;


        public UtenteController(InterfaceRepo<Utente> repUte)
        {
            _repositoryUte = repUte;
        }






        public IActionResult ListaUtenti()
        {
            List<Utente> elenco = (List<Utente>)_repositoryUte.GetAll();

            ViewBag.Title = "Lista Utenti";

            return View(elenco);
        }



        
        public IActionResult Login()
        {
            return View();

        }

        [HttpPost]
        private IActionResult Login(Utente objUte)
        {
                       

            return Ok(objUte);

        }





        [HttpPost]
        public IActionResult Iscrizione(Utente objUte)
        {
            if (ModelState.IsValid)
            {
                _repositoryUte.Insert(objUte);
                return Redirect("/");
            }
            else
            {

                return View(objUte);
            }

        }








        public IActionResult Dettaglio(string varCod_U)
        {
            Utente temp = _repositoryUte.GetFromCodice(varCod_U);

            if (temp == null)
                return Redirect("Utente/Errore");

            return View(temp);
        }





        [HttpPost]
        public IActionResult Modifica(Utente objUte)
        {

            //TODO...
            return View();
        }





        [HttpDelete]
        public ActionResult Elimina(string varCod_U)
        {
            //TODO...
            return Ok(new { Status = "success" });
        }







    }
}
