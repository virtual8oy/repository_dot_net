﻿using Microsoft.AspNetCore.Mvc;
using Palestra_ASP.Data;
using Palestra_ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP.Controllers
{
    public class CorsoController : Controller
    {
        private readonly InterfaceRepo<Corso> _repositoryCor;
        



        public CorsoController(InterfaceRepo<Corso> repCor)
        {
            _repositoryCor = repCor;
        }






        //[HttpPost]
        public IActionResult ListaCorsi()
        {
            List<Corso> elenco = (List<Corso>)_repositoryCor.GetAll();

            ViewBag.Title = "Lista Corsi";

            return View(elenco);
        }












        [HttpPost]
        public IActionResult Inserisci(Corso objCor)
        {

            if (ModelState.IsValid)
            {
                _repositoryCor.Insert(objCor);
                return Redirect("/");
            }
            else
            {

                return View(objCor);
            }

        }








        public IActionResult Dettaglio(string varCod_C)
        {

            Corso temp = _repositoryCor.GetFromCodice(varCod_C);

            if (temp == null)
                return Redirect("Corso/Errore");

            return View(temp);
        }








        [HttpPost]
        public IActionResult Modifica(Corso objCor)
        {

            //TODO...
            return View();
        }







        [HttpDelete]
        public ActionResult Elimina(string varCod_C)
        {
            //TODO...
            return Ok(new { Status = "success" });
        }

    }
}
