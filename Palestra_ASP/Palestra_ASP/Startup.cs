using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Palestra_ASP.Data;
using Palestra_ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Palestra_ASP
{
    public class Startup
    {

        private IConfiguration configuration;

        public Startup(IConfiguration config)
        {
            configuration = config;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddScoped<InterfaceRepo<Corso>, CorsoRepo>();
            services.AddScoped<InterfaceRepo<Utente>, UtenteRepo>();

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(
                    configuration.GetConnectionString("DbLocale")
                ));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Corso}/{action=ListaCorsi}/{varCod_C?}");
            });
        }
    }
}
