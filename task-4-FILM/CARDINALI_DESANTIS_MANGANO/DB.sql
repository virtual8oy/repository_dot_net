CREATE DATABASE boxoffice;
USE boxoffice;

CREATE TABLE Film(
	id INT PRIMARY KEY IDENTITY(1,1),
	title VARCHAR(255) NOT NULL,
	image VARCHAR(255),
	description TEXT,
	director VARCHAR(255),
	producer VARCHAR(255),
	releaseDate INT,
	runningTime INT,
	profits BIGINT,
);