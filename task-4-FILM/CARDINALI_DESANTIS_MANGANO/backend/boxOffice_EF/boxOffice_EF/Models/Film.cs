﻿using System;
using System.Collections.Generic;

#nullable disable

namespace boxOffice_EF.Models
{
    public partial class Film
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public string Producer { get; set; }
        public int? ReleaseDate { get; set; }
        public int? RunningTime { get; set; }
        public long Profits { get; set; }
    }
}
