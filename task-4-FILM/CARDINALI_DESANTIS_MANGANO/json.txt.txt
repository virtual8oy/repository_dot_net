[
{
    "title": "Avatar",
    "image": "https://xl.movieposterdb.com/09_12/2009/499549/xl_499549_23ca4c37.jpg",
    "description": "L'ex marine Jake Sully è stato reclutato per una missione sul pianeta Pandora con lo scopo di recuperare risorse naturali in esaurimento sulla Terra. Inaspettatamente si ritrova a voler proteggere il mondo magico al quale si sente stranamente legato.",
    "director": "James Cameron",
    "producer": "James Cameron",
    "release_date": 2009,
    "running_time": 162,
    "profits": 2847379794
  },
  {
    "title": "Avengers: Endgame",
    "image": "https://xl.movieposterdb.com/20_05/2019/4154796/xl_4154796_2542f70b.jpg",
    "description": "Alla deriva nello spazio senza cibo o acqua, Tony Stark vede la propria scorta di ossigeno diminuire di minuto in minuto. Nel frattempo, i restanti Vendicatori affrontano un epico scontro con Thanos.",
    "director": "Anthony Russo, Joe Russo",
    "producer": "Kevin Feige",
    "release_date": 2019,
    "running_time": 181,
    "profits": 2797501328
  },
  {
    "title": "The Lion King",
    "image": "https://xl.movieposterdb.com/22_02/2019/6105098/xl_6105098_3c46c8c0.jpg",
    "description": "Tradito dallo zio che ha ordito un terribile complotto per prendere il potere, il piccolo Simba, leoncino figlio del re della foresta, affronta il proprio destino nel cuore della savana.",
    "director": "Jon Favreau",
    "producer": "Jon Favreau",
    "release_date": 2019,
    "running_time": 118,
    "profits": 1662899439
  },
  {
    "title": "Black Panther",
    "image": "https://xl.movieposterdb.com/21_11/2018/1825683/xl_1825683_56626bcd.jpg",
    "description": "Il giovane principe T'Challa ritorna a casa, nella nazione tecnologicamente avanzata del Wakanda. Ben presto però è costretto a richiedere l'aiuto dell'agente della CIA Everett K. Ross per difendere il trono ed evitare una guerra civile.",
    "director": "Ryan Coogler",
    "producer": "Kevin Feige",
    "release_date": 2018,
    "running_time": 134,
    "profits": 1347597973
  },
  {
    "title": "The Lord of the Rings: The Return of the King",
    "image": "https://xl.movieposterdb.com/07_12/2003/167260/xl_167260_6ffc2af3.jpg",
    "description": "Mentre Frodo e Sam, accompagnati da Gollum, proseguono il loro viaggio verso Monte Fato per distruggere l'anello, il resto della compagnia corre in soccorso di Rohan e Gondor, impegnati nella battaglia dei Campi del Pellenor.",
    "director": "Peter Jackson",
    "producer": "Barrie M. Osborne",
    "release_date": 2003,
    "running_time": 200,
    "profits": 1146436214
  }
]
