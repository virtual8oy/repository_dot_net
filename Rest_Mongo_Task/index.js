const express = require('express')
const mongoose = require('mongoose')

const Prodotti = require('./models/Prodotti')
const Categorie = require('./models/Categorie')


const bodyParser = require('body-parser')
const app = express()
const porta = 4001
const dbName = "Ristorante"

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))


const db = mongoose.connect(`mongodb+srv://Virtual8oy:asdasd@cluster0.cznut.mongodb.net/test${dbName}?retryWrites=true&w=majority`, {useNewUrlParser: true}, () => {
    console.log("Sono connesso!")
})

app.listen(porta, () => {
    console.log(`Sono in ascolto sulla porta ${porta}`)
})



app.get("/api/prodotti", (req, res) => {
    Prodotti.find({}, (error, elenco) => {
        if(!error)
            res.json(elenco)
        else
            res.status(500)
    })
})

app.get("/api/categorie", (req, res) => {
    Categorie.find({}, (error, elenco) => {
        if(!error)
            res.json(elenco)
        else
            res.status(500)
    })
})

app.get("/api/prodotto/:idprodotto", (req, res) => {
    console.log(req.params.idprodotto)

    Prodotto.findById(req.params.idprodotto, (error, item) => {
        if(!error)
            res.status(200).json(item)
        else
            res.status(500).end()
    })
})

app.get("/api/categorie/:idcategorie", (req, res) => {
    console.log(req.params.idcategorie)

    Categorie.findById(req.params.idcategorie, (error, item) => {
        if(!error)
            res.status(200).json(item)
        else
            res.status(500).end()
    })
})




app.delete("/api/prodotti/:idprodotto", (req, res) => {
    console.log(req.params.idprodotto)

    Prodotti.findByIdAndDelete(req.params.idprodotto, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"})
        else
            res.status(500).end()
    })
})

app.get("/api/categorie/:idcategorie", (req, res) => {
    console.log(req.params.idcategorie)

    Categorie.findByIdAndDelete(req.params.idcategorie, (error, item) => {
        if(!error)
            res.status(200).json(item)
        else
            res.status(500).end()
    })
})


app.post("/api/prodotti/inserisci", (req, res) => {
    console.log(req.body)

    Prodotti.create(req.body, (error, item) => {
        if(!error)
            res.status(200).json(item)
        else
            res.status(500).end()
    })
})

app.post("/api/categorie/inserisci", (req, res) => {
    console.log(req.body)

    Categorie.create(req.body, (error, item) => {
        if(!error)
            res.status(200).json(item)
        else
            res.status(500).end()
    })
})


app.put("/api/prodotti/:idprodotti", (req, res) => {
    console.log(req.params.idprodotti)

    Prodotti.findByIdAndUpdate(req.params.idprodotti, req.body, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"})
        else
            res.status(500).end()
    })
})

app.put("/api/categorie/:idcategorie", (req, res) => {
    console.log(req.params.idcategorie)

    Student.findByIdAndUpdate(req.params.idcategorie, req.body, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"})
        else
            res.status(500).end()
    })
})