const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductSchema = new Schema({
    nome: String,
    quantita: String,
    matricola: String
})

const Prodotti = mongoose.model('Prodotti', ProductSchema)

module.exports = Prodotti