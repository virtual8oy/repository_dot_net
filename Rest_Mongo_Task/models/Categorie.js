const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CategorSchema = new Schema({
    nome: String,
    quantita: String,
    matricola: String
})

const Categorie = mongoose.model('Categorie', CategorSchema)

module.exports = Categorie