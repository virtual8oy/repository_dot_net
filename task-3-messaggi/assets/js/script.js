let input_Nickname = $("#input_nickname").val();

function salvaNick(){
    let input_Nickname = $("#input_nickname").val();

}

function salvaContatto(){
    let input_Nickname = $("#input_nickname").val();
    // let input_Testo = $("#input_testo").val();
    // let input_Tempo = $("#input_tempo").val();

    let contatto = {
        nickname: input_Nickname,
        // testo: input_Testo,
        // tempo: input_Tempo
    }

    $.ajax(
        {
            url: "",
            method: "POST",
            data: JSON.stringify(contatto),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "success":
                        stampaTabella();
        
                        $("#input_nickname").val("");
                        $("#input_testo").val("");
                        $("#input_tempo").val("");
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

function stampaTabella(){
    $.ajax(
        {
            url: "",
            method: "GET",
            success: function(response){

                let contenuto = "";
                for(let [index, item] of response.entries()){
                    contenuto += `  <tr>
                                        <td>${item.nickname}</td>
                                        <td>${item.testo}</td>
                                        <td>${item.tempo}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" onclick="dettaglioContatto(${item.id})">
                                                <i class="fa-solid fa-magnifying-glass-plus"></i>
                                            </button>
                                            
                                            <button type="button" class="btn btn-danger" onclick="eliminaContatto(${item.id})">
                                                <i class="fa-solid fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>`;
                }

                $("#contenuto-tabella").html(contenuto);

            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

$(document).ready(function(){
    stampaTabella();
    $("#btnNick").click(function(){
        salvaNick();
    })
    $("btnInvio").click(function(e){
        salvaMessaggio();

    });
});