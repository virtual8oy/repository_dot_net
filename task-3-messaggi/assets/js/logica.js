// function stampaTabella(){
//     $.ajax(
//         {
//             url: "http://localhost:54485/api/chat",
//             method: "GET",
//             success: function(response){

//                 let contenuto = "";
//                 for(let [index, item] of response.entries()){
//                     contenuto += `  <tr>
//                                         <td>${item.nickname}</td>
//                                         <td>${item.testo}</td>
//                                         <td>${item.tempo}</td>
//                                         <td>
//                                             <button type="button" class="btn btn-primary" onclick="dettaglioContatto(${item.id})">
//                                                 <i class="fa-solid fa-magnifying-glass-plus"></i>
//                                             </button>
                                            
//                                             <button type="button" class="btn btn-danger" onclick="eliminaContatto(${item.id})">
//                                                 <i class="fa-solid fa-trash"></i>
//                                             </button>
//                                         </td>
//                                     </tr>`;
//                 }

//                 $("#contenuto-tabella").html(contenuto);

//             },
//             error: function(errore){
//                 console.log(errore)
//                 alert("Errore")
//             }
//         }
//     );
// }




function salvaNick()
{
    let input_Nickname = $("#nickname").val();

    localStorage.setItem(input_Nickname);

}

// function salvaMessaggio()
// {
//     let input_Testo = $("#messagebox").val();


// }








function salvaNick(){
    let input_Nickname = $("#nickname").val();
    

    let contatto = {
        nickname: input_Nickname,
        
    }

    $.ajax(
        {
            url: "http://localhost:54485/api/index",
            method: "GET",
            data: JSON.stringify(contatto),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "success":
                        stampaTabella();
        
                        $("#input_nickname").val("");
                        
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}


$(document).ready(function()
{
    stampaTabella();

    $("#btnNick").click(function()
    {
        salvaNick();
    })

    $("btnInvio").click(function()
    {
        salvaMessaggio();

    });
});

function stampaChat(){
    $.ajax(
        {
            url: "https://localhost:54485/api/chat/",
            method: "GET",
            success: function(response){
                let contenuto = "";
                for(let item of response)
                {
                    let orario = item.orario.hours+":"+item.orario.minutes+":"+item.orario.seconds;
                    contenuto += `
                        <li style="list-style-type: none">
                            <p class="">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <strong>${item.nickname}</strong>
                                <small>${tempo}</small>
                            </p> 
                            <p>${item.testo}</p>
                        </li>
                        <hr />
                    `;
                }
                $("#itemContent").html(contenuto);
            }
        }
    )
}



function inviaMessaggio(){
    let username = nickname;
    
    let testo = $("#messagebox").val();

    let messaggio = {
        utente: username,
        orario: tempo,
        messaggio: testo,
    }

    $.ajax(
        {
            url: "https://localhost:54485/api/chat",
            method: "POST",
            data: JSON.stringify(messaggio),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "Success":
                        $("#contenuto-tabella").val("");
                        break;
                    case "Error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            errore: function(errore){
                console.log(errore);
                alert("Errore");
            }
        }
    );
}

$(document).ready(
    function(){
       
        let autoUpdate = setInterval(function(){
            stampaChat();
        },100);

        console.log(localStorage.getItem("chat_nickname"));
        setTimeout(function(){
            if(localStorage.getItem("chat_nickname") == "[]" && window.location.href == "http://127.0.0.1:5500/chat.html")
            {
                 $(window.location).attr('href', 'index.html');
            }
        },1000);
    }   
)