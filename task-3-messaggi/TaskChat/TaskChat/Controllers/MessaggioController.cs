﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskChat.Data;
using TaskChat.Models;

namespace TaskChat.Controllers
{
    [Route("api/chat")]
    [ApiController]
    public class MessaggioController : Controller
    {
        private readonly SqlMessaggio _repository = new SqlMessaggio();

        [HttpPost,HttpGet]
        public ActionResult<IEnumerable<Messaggio>> getAllMessaggi()
        {
            var elenco = _repository.GetAll();
            return Ok(elenco);
        }

        [HttpPost("insert")]
        public ActionResult InsertMessaggio(Messaggio obj)
        {
            if (_repository.Insert(obj))
                return Ok("Successo!");
            else
                return Ok("Errore, non ho completato l'operazione");
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteMessaggio(int id)
        {
            if (_repository.Delete(id))
                return Ok("Successo!");
            else
                return Ok("Errore, non ho completato l'operazione");
        }
        [HttpPut("{id}")]
        public ActionResult UpdateMessaggio(Messaggio obj, int id)
        {
            obj.Id = id;

            if (_repository.Update(obj))
                return Ok("Successo!");
            else
                return Ok("Errore, non ho completato l'operazione");
        }
    }
}
