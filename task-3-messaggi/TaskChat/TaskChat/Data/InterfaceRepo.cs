﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskChat.Data
{
    interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();

        bool Insert(T obj);

        bool Delete(int varId);

        bool Update(T obj);
    }
}
