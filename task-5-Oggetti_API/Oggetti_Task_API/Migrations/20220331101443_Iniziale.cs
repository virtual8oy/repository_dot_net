﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Oggetti_Task_API.Migrations
{
    public partial class Iniziale : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "db_oggetti",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Descrizione = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Codice = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Quantita = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_db_oggetti", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_db_oggetti_Codice",
                table: "db_oggetti",
                column: "Codice",
                unique: true,
                filter: "[Codice] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "db_oggetti");
        }
    }
}
