﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oggetti_Task_API.Models
{
    public class Risposta
    {
        public string Status { get; set; }
        public string Descrizione { get; set; }
    }
}
