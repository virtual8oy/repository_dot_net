﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Oggetti_Task_API.Models
{
    [Table("db_oggetti")]
    [Index(nameof(Codice), IsUnique = true)]
    public class Oggetto
    {
        [Key]
        public int Id { get; set; }

        
        [Required]
        [MaxLength(250)]
        public string Nome { get; set; }

        [MaxLength(500)]
        public string Descrizione { get; set; }

        [Required]
        [MaxLength(50)]
        public string Codice { get; set; }

        [Required]
        public int Quantita { get; set; }

    }
}
