﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oggetti_Task_API.Data
{
    public interface InterfaceRepo<T>
    {

        bool Insert(T obj);
        IEnumerable<T> GetAll(); //prendo tutti

        T GetById(int varId); //il singolo elemento id limitato ad esso

        bool UpDate(T obj);


        bool Delete(int varId);

        

        T FindByCode(string varCodice);
    }
}
