﻿using Oggetti_Task_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oggetti_Task_API.Data
{
    public class OggettiRepo : InterfaceRepo<Oggetto>
    {

        private readonly OggettiContext _context;



        public OggettiRepo(OggettiContext con)
        {
            _context = con;

        }



        #region Delete
        public bool Delete(int varId)
        {
            try
            {
                Oggetto temp = _context.Oggetti.Where(o => o.Id == varId).First();
                _context.Oggetti.Remove(temp);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion



        #region FindByCode
        public Oggetto FindByCode(string varCodice)
        {
            return _context.Oggetti
                    .Where(o => o.Codice.ToLower().Equals(varCodice.ToLower()))
                    .FirstOrDefault();
        }
        #endregion




        #region GetAll
        public IEnumerable<Oggetto> GetAll()
        {
            return _context.Oggetti.ToList();

        }
        #endregion




        #region GetById
        public Oggetto GetById(int varId)
        {
            throw new NotImplementedException();
        }

        #endregion




        #region Insert
        public bool Insert(Oggetto obj)
        {
            try
            {
                _context.Oggetti.Add(obj);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion


        #region Update
        public bool UpDate(Oggetto obj)
        {
            try
            {
                _context.Oggetti.Update(obj);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        //Seguendo il task limita la cancellazione
        //public bool DeleteByCode


    }
}
