﻿using Microsoft.EntityFrameworkCore;
using Oggetti_Task_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oggetti_Task_API.Data
{
    public class OggettiContext : DbContext
    {
        public OggettiContext(DbContextOptions<OggettiContext> opt) : base(opt) //Il mio ctruttore costruirà il mio db context
        {

        }

        public DbSet<Oggetto> Oggetti { get; set; }


    }
}
