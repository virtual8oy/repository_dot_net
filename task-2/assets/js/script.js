function scambia() {
    $('#tab1').toggleClass('nascondi');
    $('#tab2').toggleClass('nascondi');
}


function inserimentoBrani(){
    $('#tab1').toggleClass('nascondi');
}

function listaBrani(){
    $('#tab2').toggleClass('nascondi');
}

function eliminaBrani(){
    localStorage.clear();
}



//Inserimento Dati Pagina Principale
function inserisci(){
    
    let varTitolo = document.getElementById("inTitolo").value;
    let varAutore = document.getElementById("inAutore").value;
    let varAnno = document.getElementById("inAnno").value;

    let prod = {
        id: calcolaId(),
        titolo: varTitolo,
        autore: varAutore,
        anno: varAnno,
    }

    if( varTitolo.length == 0 ||
        varAutore.length == 0 ||
        varAnno.length == 0){
        alert("Non puoi inserire un prodotto inesistente!");
        return;
    }

    if(isInElenco(varTitolo)){
        alert("Non puoi inserire un prodotto già esistente!");
        return;
    }

    elenco.push(prod);
    localStorage.setItem("JukeBox 2.0", JSON.stringify(elenco));
    alert("Brano registrato")

    document.getElementById("inTitolo").focus()
}



//Verifica Codice doppione
function isInElenco(varTitolo){
    for(let [index, item] of elenco.entries()){
        if(item.titolo == varTitolo)
            return true;
    }

    return false;
}



//Generatore di ID
function calcolaId(){
    if(elenco.length != 0)
        return elenco[elenco.length - 1].id + 1;

    return 1;
}



//Bottone Elimina Articoli
function elimina(varId){
    for(let [index, item] of elenco.entries()){
        if(item.id == varId)
            elenco.splice(index, 1)
    }

    localStorage.setItem("JukeBox 2.0", JSON.stringify(elenco));
    stampaProdotti();
}




//Bottone Apertura Modale
function apriModale(varId){
    for(let [index, item] of elenco.entries()){
        if(item.id == varId){
            document.getElementById("update_id").value = item.id;
            document.getElementById("update_titolo").value = item.titolo;
            document.getElementById("update_autore").value = item.autore;
            document.getElementById("update_anno").value = item.anno;
        }
    }

    $("#modaleModifica").modal('show');
}




//Bottone Chiusura Modale
function chiudi(){
    $("#modaleModifica").modal('hide');

    stampaProdotti();
}



//Bottone Modifica Dati
function aggiorna(){
    let var_Id = document.getElementById("update_id").value;
    let var_Titolo = document.getElementById("update_titolo").value;
    let var_Autore = document.getElementById("update_autore").value;
    let var_Anno = document.getElementById("update_anno").value;

    let oggetto = {
        id: var_Id,
        titolo: var_Titolo,
        autore: var_Autore,
        anno: var_Anno,
    }

    for(let [index, item] of elenco.entries()){
        if(item.id == oggetto.id){
            item.titolo = oggetto.titolo;
            item.autore = oggetto.autore;
            item.anno = oggetto.anno;

            alert("Modifica Eseguita");
        }
    }
    
    $("#modaleModifica").modal('hide');

    stampaProdotti();
}




//Lista Articoli
function stampaProdotti(){
    let contenuto = "";

    for(let [index, item] of elenco.entries()){
        contenuto += `
            <tr>
                <td>${item.id}</td>
                <td>${item.titolo}</td>
                <td>${item.autore}</td>
                <td>${item.anno}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="elimina(${item.id})">
                        <i class="fa-solid fa-trash text-danger"></i>
                    </button>
                    <button type="button" class="btn btn-outline-warning" onclick="apriModale(${item.id})">
                        <i class="fa-solid fa-pencil text-warning"></i>
                    </button>
                </td>
            </tr>
        `;
    }

    
    document.getElementById("contenuto-tabella").innerHTML = contenuto;

}




if(localStorage.getItem("JukeBox 2.0") == null)
    localStorage.setItem("JukeBox 2.0", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("JukeBox 2.0"));




if(document.getElementById("contenuto-tabella") != null)
    stampaProdotti();



